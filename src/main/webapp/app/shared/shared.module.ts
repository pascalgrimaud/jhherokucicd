import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { JhherokucicdSharedLibsModule, JhherokucicdSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [JhherokucicdSharedLibsModule, JhherokucicdSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [JhherokucicdSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JhherokucicdSharedModule {
  static forRoot() {
    return {
      ngModule: JhherokucicdSharedModule
    };
  }
}
